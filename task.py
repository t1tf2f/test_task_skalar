def biggestPath(X: dir) -> str:
    result = []
    for i in X:
        # Проверка на англ
        if isValidated(i):
            # Вызов рекурсивной функции поиска путей
            recursiveAddPaths(X[i], i, result, 1)
        else:
            return '/'
    max_path = '/'
    max_path_count = 0
    # Поиск самого длинного пути в массиве
    for i in result:
        if 'error' in i:
            return '/'
        elif i['count'] >= max_path_count:
            max_path = i['path']
            max_path_count = i['count']
    if len(max_path) > 255:
        max_path = '/'
    return max_path


def isValidated(string):
    # Перебор символов в строке
    for char in string:
        # Проверка номер символа в юникоде
        if ord(char) > 127:
            return False
    return True

def recursiveAddPaths(X, path: str, result: list, count: int):
    # Проверка если объект типа list(массив файлов)
    if type(X) == list:
        temp_list = []
        for i in X:
            # Проверка на дубликат и англ символы
            if i not in temp_list and isValidated(i):
                temp_list.append(i)
            else:
                result.append({'error': True})
    # Проверка если объект типа dict(дериктории)
    elif type(X) == dict:
        count += 1
        if len(X) == 0:
            # Добавлдение путя в массив со всеми дерикториями
            result.append({"path": '/' + path, 'count': count})
        else:
            # Перебор всех поддеректорий
            for i in X:
                # Проверка на англ символы в пути
                if isValidated(i):
                    # Добавление пути в массив
                    result.append({"path": '/' + path + '/' + i, 'count': count})
                    # Рекурсивный вызов функции
                    recursiveAddPaths(X[i], path + '/' + i, result, count)
                else:
                    # Путь не из англ символов, запись ошибки в массив с результатами
                    result.append({'error': True})
    else:
        result.append({"path": '/' + path, 'count': count})
